package jp.co.ayuta.example.ytdl

import org.apache.commons.lang3.StringUtils
import org.jsoup.Jsoup
import java.io.File
import java.net.URL
import java.util.*

fun main(args: Array<String>) {
    if (args.size == 0) {
        println("YouTubeのURLを指定してください.")
        return
    }

    val url: String = args[0]
    if (!url.startsWith("http")) {
        println("httpsから始まるURLを指定してください")
        return
    } else if (url.indexOf("playlist") < 0) {
        println("プレイリストのURLを指定してください")
        return
    }

    // 指定がない場合、カレントディレクトリ
    var dlPath = "."
    if (args.size == 2) {
        val argPath = args[1]
        if (argPath.startsWith(".") || argPath.startsWith("/") || argPath.startsWith("~")) {
            dlPath = argPath
        }
    }

    val ld = ListDownload(URL(url), File(dlPath))
    ld.download()
}

/**
 * プレイリストを指定してダウンロードする
 */
class ListDownload(val playListUrl: URL, val dlPath: File) {

    private val YOUTUBE_URL = "https://www.youtube.com"

    /**
     * プレイリストの動画をダウンロードする
     */
    fun download() {
        val urls = findLink()
        print("Downloading")
        for (url in urls) {
            DirectDownload(URL(url), dlPath).download()
        }
        println("\nFinish!")
    }

    /**
     * プレイリストの中から動画URLを見つけて返す
     *
     * @return 動画URL一覧
     */
    private fun findLink(): List<String> {
        val doc = Jsoup.parse(playListUrl, 5 * 1000)
        val elements = doc.select("a.yt-uix-tile-link")

        val urls = ArrayList<String>()
        for (element in elements) {
            val url = element.attributes().get("href")
            url?.let {
                if (StringUtils.isNotBlank(url)) {
                    urls.add(YOUTUBE_URL + url)
                }
            }
        }
        return urls
    }

}