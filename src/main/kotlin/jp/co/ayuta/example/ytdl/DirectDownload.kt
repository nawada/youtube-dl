package jp.co.ayuta.example.ytdl

import com.github.axet.vget.VGet
import java.io.File
import java.net.MalformedURLException
import java.net.URL

fun main(args: Array<String>) {
    if (args.size == 0) {
        println("YouTubeのURLを指定してください.")
        return
    }

    val url: String = args[0]
    if (!url.startsWith("http")) {
        println("httpから始まるURLを指定してください")
        return
    }

    // 指定がない場合、カレントディレクトリ
    var dlPath = "."
    if (args.size == 2) {
        val argPath = args[1]
        if (argPath.startsWith(".") || argPath.startsWith("/") || argPath.startsWith("~")) {
            dlPath = argPath
        }
    }

    print("Downloading..")
    DirectDownload(URL(url), File(dlPath)).download()
    println("\nFinish!")
}

/**
 * 直接ダウンロード
 */
class DirectDownload(val url: URL, val dlPath: File) {

    /**
     * ダウンロード実行
     */
    fun download() {
        try {
            val v: VGet = VGet(url, dlPath)
            print('.')
            v.download()
        } catch(e: MalformedURLException) {
            e.printStackTrace()
        }
    }

}
